       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL1.
       AUTHOR. NATTHAKRITTA

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 NUM1  PIC   9(3).
       01 NUM2  PIC   9(3).
       01 NUM3  PIC   9(3).

       PROCEDURE DIVISION.
       BEGIN.
           IF NUM1 < 10 THEN
              DISPLAY "NUM1 < 1O"
           END-IF 

           DISPLAY ""
           IF NUM1 LESS THAN 10 
              DISPLAY "NUM1 < 1O"
           END-IF

           DISPLAY ""
           IF NUM1 GREATER THAN OR EQUAL NUM2 THEN
              MOVE NUM1 TO NUM2
           END-IF
           DISPLAY "NUM1: " NUM1 
           DISPLAY "NUM2: " NUM2 

           DISPLAY ""
           IF NUM1 < (NUM2 +(NUM3 / 2)) THEN
              MOVE ZERO TO NUM1 
           END-IF
           DISPLAY "NUM1 : " NUM1.